# Skinetic Technical Demonstrations User Guide

Welcome to the **Skinetic Technical Demonstrations User Guide**. This repository aims to provide you with all the tools, requirements, and troubleshooting methods to help you start with Actronika's Skinetic vest demonstrations.

## Available demonstrations:
| Name  | Version|  Modality | Connection Type  | Hardware needed  | Other comments  |
|---|---|---|---|---|---|
| [Technical Demonstration (PC version)](TechnicalDemonstration_PC) | 0.5.0 | VR and Non-VR  | USB, Bluetooth, Wifi  | <ul><li>PC</li><li> VR headset</li><li> Skinetic Vest</li></ul>  |   |
| [Technical Demonstration (Standalone version)](TechnicalDemonstration_Standalone/)  | 0.4.4| VR  |  USB, Bluetooth |<ul><li>Standalone VR headset</li><li>Skinetic vest</li><li>Android device with wifi network (optional)</li></ul> | (Optional) [PC](TechnicalDemonstration_Standalone/ControlApp/PC/) or [android](TechnicalDemonstration_Standalone/ControlApp/Android/) application for demo control  |


## Troubleshooting
Please refer to the [troubleshooting section](Troubleshooting.md) if you encounter any issues while using the demonstrations.

# Skinetic Technical Demo V0.5.0 Manual (PC version)
This manual is for the PC version of the **Skinetic Technical Demonstration**.

## Installation
To launch the demonstration on your computer, you will need to connect your VR headset to the computer (Optional):

* STEAM + STEAM VR (https://store.steampowered.com/about/) (https://www.steamvr.com/fr/) for Vive headsets

 OR 
 
* OCULUS (https://www.oculus.com/download_app/?id=1582076955407037) for meta headsets.


And you will need the [The demonstrator executable](Demonstration)

To connect your headset to your computer please follow these guidelines:
* For Vive headsets: https://www.vive.com/fr/solution/vive-streaming/
* For occulus headsets: https://www.meta.com/fr-fr/help/quest/articles/headsets-and-accessories/oculus-link/set-up-link/

## Set-up Timeline
The vest must be connected to the power supply and to the headset / computer via usb, bluetooth or wifi.

Bluetooth is recommended as it is the most stable wireless option with the demonstration.

1. Close any application that uses the Skinetic vest
2. Start the skinetic vest
3. Start the demonstration on the computer. 
    * The vest should connect automatically. It will first scan for skinetic devices, then connect. If it doesn't find any device or can’t connect, see the troubleshooting sections
4. The demonstration displays “waiting for the demo to start” and some instructions
    * Put the headset on the user’s head and adjust the vest. The demo is ready to start
5. Start the demo either by:
    * Pressing button A or B on an headset controller
    * Pressing ‘space’ on the keyboard
6. When the demo is finished, you can restart using buttons A or B on a headset controller, the space bar on a keyboard or the “restart” button on the remote. 
    * You can then restart on the 4th point for the next user

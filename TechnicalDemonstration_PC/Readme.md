# Technical Demonstration (PC version)
This technical demonstration of the **Skinetic Vest** aims to showcase the vest's various capabilities. It simulates sensations ranging from bullet impacts and violent explosions to gentle rain and wind.

## User Interactivity
This demonstration does not require any interaction from the user. There is no need for controllers or movement.

The user can turn around to test spatial effects, but cannot avoid bullets, as they will always be aimed at the player.

## Non-VR Version
This demonstration is compatible with non-VR setups. If no headset is connected to the computer, the camera will switch to a spectator position.

## Help
You can find a [manual](Manual.md) and a list of the [shortcuts](Shortcuts.md) in this folder. If you have any problems, please see the [troubleshooting guide](../Troubleshooting.md).
# Skinetic Technical Demonstration V0.5.0
## Shortcuts (keyboard)

While demo is running:

* Play/Restart: Spacebar
* Previous/next:  interaction: arrows ⬅️ / ➡️
* Increase/decrease global actuators volume: arrows ⬆️/⬇️ (default value at 100%)
* Display network debug console: D

## Shortcuts (controllers)

While demo is running:

* Play/Restart : A, B
* Display network debug console:  X,Y

# TroubleShooting guide for Skinetic demonstrations
This document aimes to help you fix the most known problems with the use of our demonstrations and vests.

**Table of contents:**
1. [Skinetic Vest related issues](#skinetic-vest-related-issues)
    * [No haptic effects on the vest](#no-haptic-effects-on-the-vest)
1. [Vr related issues](#vr-related-issues)
    * [Demonstration not starting in Quest Link](#demonstration-not-starting-in-quest-link)
    * [Headset not detected by Steam VR](#headset-not-detected-by-steam-vr)
    * [Steam or occulus menu displayed in front of the demonstration](#steam-or-occulus-menu-displayed-in-front-of-the-demonstration)
    * [White flashes occur in the headset](#white-flashes-occur-in-the-headset)
    * [Connect the headset to wifi](#connect-the-headset-to-wifi)
1. [Demonstration related issues](#demonstration-related-issues)
    * [Remote don’t connect to the headset](#remote-dont-connect-to-the-headset)

## Skinetic Vest related issues
### No haptic effects on the vest
Try the following solutions in this order:
* Make sure the vest is plugged in and turned on (the led must be green).
* Make sure the vest is paired with the headset or computer or connected to the same wifi or connected by USB to the computer
* Make sur the vest IS NOT selected as the computer's audio output.
* Close every application that could use the vest (such as Unitouch studio or an other demonstration)
* Change the USB port.
* Reboot the vest and the application

## Vr related issues
### Demonstration not starting in Quest Link
**Please, try the following solutions in this order**:
* Make sure everything is properly connected.
    * For occulus headsets: https://www.meta.com/fr-fr/help/quest/articles/headsets-and-accessories/oculus-link/set-up-link/
If these checks have been made and the headset is still not detected, restart it by unpluging the headset and reconnect through the menue.

### Headset not detected by Steam VR
Steam VR interface should look like this :

![SteamVR](Images/SteamVR.PNG)

If not, **try the following solutions in this order**:
* Make sure everything is properly connected.
    * For Vive headsets: https://www.vive.com/fr/solution/vive-streaming/
    * For occulus headsets: https://www.meta.com/fr-fr/help/quest/articles/headsets-and-accessories/oculus-link/set-up-link/
* For Vive headsets, check that nothing is blocking the cameras and that they can see the headset and the tracker. (such as someone between the player and a camera, hairs on the tracker, cameras misaligned, etc.)
* If these checks have been made and the headset is still not detected, restart it with the Steam VR Menu : Right click on the headset icon > “Reboot the headset”



### Steam or occulus menu displayed in front of the demonstration
Exit the steam menu with a controller (every controller has a menu button, mainly under the right thumb), or the headset with the button on the side/below.

### White flashes occur in the headset
Assure nothing is blocking the cameras from seeing the headset or eachother at any time.

### Connect the headset to wifi
![QuestWifi](Images/QuestWifi.PNG)
* Open the quick menu (by pressing oculus button on the right controller)
* Select wifi
* Make sure wifi is enabled
* Select your network
* Enter the password
* The wifi should connect

## Demonstration related issues
### Remote don’t connect to the headset
<span style="color:red">For surface tablets, don’t use the embedded wifi hotspot.
</span>.

Try the following solutions in this order:
* Make sure the headset and the remote are on the same wifi network
* Restart both applications
* Change wifi network (use a private network)
* Make sure you allowed communication through networks
* Open the debug window on the headset (D on a keyboard or a grip on a controller) and select on the remote the closest IP to the one displayed in the headset (if the three first numbers are different, the connection will be impossible)

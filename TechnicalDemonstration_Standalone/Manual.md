# Skinetic Technical Demo V0.4.4 Manual (Standalone version)
This manual is for the Standalone version of the **Skinetic Technical Demonstration**.

## Installation
### Demonstration
The demonstration is made to be standalone with an XR headset. You need to have the application on your headset. 


For Oculus headsets, you can install an app using the “Meta Quest Developer Hub” or just copy the [apk](Demonstration) onto the headset files, then click on it using the headset file explorer. Say 'yes' to all requested permissions.
Meta Quest Developer Hub installation: https://www.youtube.com/watch?app=desktop&v=0PxgPX5O7_w 

### Remote controller
If you have an android smartphone/tablet or a windows tablet, you can control the demonstration through a dedicated app (change scene, verify connectivity). 

You will need to install the [APK](ControlApp/Android/):
To install the app, simply download the APK to your phone, click on it on your phone’s file explorer and install. Say 'yes' to all requested permissions.

A [PC version](ControlApp/PC/) (for surface tablets) is available.

Allow communication over private or public networks (or both) depending on the type of network you use. (A phone hotspot should be considered as a private network)

## Set-up Timeline
The vest must be connected to the power supply and to the headset via usb or bluetooth. 
Bluetooth is recommended as it is the most stable with the demo.

1. Close any application that uses the Skinetic vest
2. Start the skinetic vest
3. Start the demonstration on the computer. 
    * The vest should connect automatically. It will first scan for skinetic devices, then connect. If it doesn't find any device or can’t connect, see the troubleshooting sections
4. If you use the phone remote: Start the phone app, and click on “not connected”.
    * The phone should connect to the demonstrations, the button becomes green with “connected” displayed.
    * The control section becomes available
4. The demonstration displays “waiting for the demo to start” and some instructions
    * Put the headset on the user’s head and adjust the vest. The demo is ready to start
5. Start the demo either by:
    * Pressing button A or B on an headset controller
    * Pressing ‘space’ on the keyboard
    * If you are using the remote, press the “start” button
6. When the demo is finished, you can restart using buttons A or B on a headset controller, the space bar on a keyboard or the “restart” button on the remote.
    * You can then restart on the 4th point for the next user

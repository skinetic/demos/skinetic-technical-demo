# Skinetic Technical Demonstration V0.4.4
## Shortcuts (controllers)

While demo is running:

* Play/Restart : A, B
* Display network debug console:  X,Y

## Shortcuts (Remote)
|![Disabled](img/Disabled.PNG)   | ![Waiting](img/Waiting.PNG)  | ![Running](img/Running.PNG)   |
|---|---|---|
| Remote not connected  | Remote connected, Demo not started  | Remote connected, Demo started  |

1. Button to initiate connection and display connection status
2. Connection parameters 
3. Current scene playing on the demonstration and scene selector
4. Start button to start the demonstration
5. Restart button to restart from the beginning the demonstration
6. Vest connection status (green = connected, yellow = connecting, red = not connected)
7. Haptic effect warning (When a haptic effect is played the logo pulses un green)
8. Strength of the haptic effects (0 to 100%)
9. Haptic effect boost (-100% to 100%; half is no boost)
10. Sound volume (0 to 100%)

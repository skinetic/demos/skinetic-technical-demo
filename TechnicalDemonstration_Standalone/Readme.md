# Technical Demonstration (Standalone version)
This technical demonstration of the **Skinetic Vest** aims to showcase the vest's various capabilities. It simulates sensations ranging from bullet impacts and violent explosions to gentle rain and wind.

## User Interactivity
This demonstration does not require any interaction from the user. There is no need for controllers or movement.

The user can turn around to test spatial effects, but cannot avoid bullets, as they will always be aimed at the player.

## Remote Control App
An Android or Windows app is available (but not mandatory) to control the demonstration. This app allows you to check the vest's connectivity, see which chapter is being played, and change some settings.